Biodynapp
=============

About Project
============
Application for android mobile devices for biofeedback physical exercise. Through bluetooth, sensors and elastic exercise the user can monitor their physical activity. 

Authors and copyright
====================
AUTHORS.md - list of authors (updated at each release)
COPYRIGHT - Copyright statement for the project
